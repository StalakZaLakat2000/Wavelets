#Koeficijenti
import numpy as np
import pywt
import cv2

level = 3

def show(img):
  cv2.imshow("Coefficients", img)
  cv2.moveWindow("Coefficients", 800, 200)
  cv2.waitKey(0)
  cv2.destroyAllWindows()

def intensity_rescale(img, low=0, high=255):
  out = np.zeros(img.shape, dtype=np.float32)
  factor = ( float(high - low) / float(img.max()-img.min()) )
  out = (img - img.min()) * factor + low
  out[out>255]=255
  out[out<0]  = 0
  return out.astype(np.uint8)

pic_path= '/home/filip/Desktop/OSIRV_project/lenna5.png'
#pic_path= '/home/filip/Desktop/OSIRV_project/lenna.bmp'

img = cv2.imread(pic_path, 0)
print img.shape

#coefficients of noised image
coeffs = pywt.wavedec2(img, 'haar', level = level)

outimg = np.zeros((img.shape[0], 2*img.shape[1]), dtype=np.uint8)
outimg[:img.shape[0], :img.shape[1]] = img

cimg = np.zeros(img.shape, dtype=np.uint8)
cAn = intensity_rescale(coeffs[0])
cimg[0:cAn.shape[0], 0:cAn.shape[1]] = cAn

for lvl, (cH, cV, cD) in enumerate(coeffs[1:]):
    h = intensity_rescale(cH)
    v = intensity_rescale(cV)
    d = intensity_rescale(cD)
    cimg[0:h.shape[0], h.shape[1]:2*h.shape[1]] = h
    cimg[v.shape[0]:2*v.shape[0], 0:v.shape[1]] = v
    cimg[d.shape[1]:2*d.shape[1], d.shape[1]:2*d.shape[1]] = d

outimg[0:cimg.shape[0], cimg.shape[1]:] = cimg

cv2.imwrite("/home/filip/Desktop/OSIRV_project/sigma_5_haar_" + str(level) + ".png", outimg)
show(outimg)
