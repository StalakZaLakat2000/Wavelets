#Stvaranje suma na slikama
import numpy as np
import pywt
import cv2

path= '/home/filip/Desktop/OSIRV_project/lenna.bmp'
img = cv2.imread(path, 0)
sigma = 5
mu = 0

def regularize_image(img):
    # to grayscale
    regImg = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    # convert to float
    regImg = np.float32(regImg)
    regImg /= 255
    img = regImg
    return img

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.float32)

gauss = gaussian_noise(img, 0, sigma)
cv2.imwrite("/home/filip/Desktop/OSIRV_project/lenna" + str(sigma) + ".png", gauss)