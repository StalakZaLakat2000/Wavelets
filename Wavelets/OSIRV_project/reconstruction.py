#Rezultat uklanjanja suma sa slike
import numpy as np
import pywt
import cv2

pic_path= '/home/filip/Desktop/OSIRV_project/lenna5.png'
noiseSigma = 50
level = 3

img = cv2.imread(pic_path, 0)

coeffs = pywt.wavedec2(img, 'db8', level = level)
#Donoho-Johnstone universal threshold
threshold = noiseSigma * np.sqrt(2 * np.log2(img.size))
rec_coeffs = coeffs
#threshold
rec_coeffs[1:] = (pywt.threshold(i, value=threshold, mode="soft") for i in rec_coeffs[1:])

#reconstructing image using wavelets
rec_img = pywt.waverec2(rec_coeffs, 'db8')
cv2.imwrite("/home/filip/Desktop/OSIRV_project/reconstruction_sigma5_hdb8_" +str(level) + ".png", rec_img)

